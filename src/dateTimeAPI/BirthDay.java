package dateTimeAPI;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class BirthDay {
	public static void main(String args[]) {
	DateTimeFormatter formatter = DateTimeFormatter
		    .ofPattern("yyyy-MM-dd'T'HH:mm:ss,nnnnnnnnnZ");
		OffsetDateTime odt = OffsetDateTime.of(1995, 10, 7, 07, 38, 0, 123456789, ZoneOffset.UTC);
		System.out.println(odt.format(formatter));
	}
}
