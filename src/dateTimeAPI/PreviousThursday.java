package dateTimeAPI;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class PreviousThursday {
	public static void main(String args[]) {
		System.out.println(LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.THURSDAY)));
	}

}
