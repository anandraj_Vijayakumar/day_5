package lambdas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RemoveX {
	public List<String> noX(List<String> strings) {
		 return strings.stream().map(x -> x.replaceAll("x", "")).collect(Collectors.toList());     
    }
	
		public static void main(String args[]) {
			List<String> strings= new ArrayList<String>();
			strings.addAll(Arrays.asList("ax","bb","cx"));
			List<String> strings1= new ArrayList<String>();
			strings1.addAll(Arrays.asList("xxax","xbxbx","xxcx"));
			List<String> strings2= new ArrayList<String>();
			strings2.addAll(Arrays.asList("x"));
			RemoveX x = new RemoveX();
			System.out.println(x.noX(strings));
			System.out.println(x.noX(strings1));
			System.out.println(x.noX(strings2));
		}

}
