package lambdas;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SimpleSingleton {
	public static SimpleSingleton getInstance() {
			return getInstance();
		}
	public static void databaseQuery(BigDecimal input) throws SQLException, ClassNotFoundException {
		 try {
			Class.forName("com.mysql.jdbc.Driver");	
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/student","root","mysql");
			Statement st =conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM customer");
			int x=0;	
			while(rs.next()) {
				x = rs.getInt(1);
			}
			conn.close();
			st.close();
		}
			 
		catch(SQLException e )
	    {
	      System.err.println("Some problem in creating connection. " + e);
	    }
	   	      catch(Exception e)
	      {
	        System.err.println("Unable to close. " + e);
	      }
		}
//	}
	
	public static void main(String args[]) throws SQLException, ClassNotFoundException {
		BigDecimal x= BigDecimal.valueOf(0.0004);
		SimpleSingleton.databaseQuery(x);
		System.out.println(x);
	}

}
