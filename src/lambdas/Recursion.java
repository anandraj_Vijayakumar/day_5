package lambdas;

public class Recursion {
	public static boolean groupSumClump(int start, int[] nums, int target) {
        if (start >= nums.length) return target == 0;
        int sum = nums[start];
        int count = 1;
        for (int i = start + 1; i < nums.length; i++) {
            if (nums[i] == nums[start]) {
                sum += nums[i];
                count++;
            }
        }
        return (start < nums.length && groupSumClump(start + count, nums, target - sum)) ||
                (start < nums.length && groupSumClump(start + count, nums, target));
    }
	public static void main(String args[]) {
		int[] nums = {2,4,8};
		int[] nums1= {1,2,4,8,1};
		int[] nums2= {2,4,4,8};
		System.out.println(Recursion.groupSumClump(0,nums,14));
		System.out.println(Recursion.groupSumClump(0,nums1,14));
		System.out.println(Recursion.groupSumClump(0,nums2,14));
		
		
	}
	

}
