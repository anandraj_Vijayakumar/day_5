package lambdas;

import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class PerformOperations {
	interface EvenOdd {
		void check(int a);
	}

	public static void main(String[] args) {
		// Lambda Expression
		EvenOdd evenOdd = (int a) -> {
			if (a % 2 == 0) {
				System.out.println("EVEN");
			} else {
				System.out.println("ODD");
			}
		};
		evenOdd.check(8);
		if (isPrime(7) == true) {
			System.out.println("Prime");
		} else {
			System.out.println("Not Prime");
		}
		if (isPalindrome(555) == true) {
			System.out.println("Palindrome");
		} else {
			System.out.println("Not Palindrome");
		}
		evenOdd.check(5);
		if (isPrime(9) == false) {
			System.out.println("Composite");
		} else {
			System.out.println("Not Composite");
		}
	}

	public static boolean isPrime(final long number) {
		return LongStream.range(2, (long) Math.ceil(Math.sqrt(number + 1))).noneMatch(x -> number % x == 0);
	}

	public static boolean isPalindrome(int number) {

		return number == IntStream.iterate(number, i -> i / 10).map(n -> n % 10).limit(String.valueOf(number).length())
				.reduce(0, (a, b) -> a = a * 10 + b);
	}

}
