package lamdaFunctionalInrefaceStream;

interface EvenOdd {
	void check(int a);
}

public class CheckOddEven {
	public static void main(String[] args) {
		// Lambda Expression
		EvenOdd evenOdd = (int a) -> {
			if(a%2==0){
				System.out.println("e"+a);
			}else{
				System.out.println("o"+a);
			}
		};
		
		//Check numbers
		evenOdd.check(3);
		evenOdd.check(44);
	}
	
}
