package lamdaFunctionalInrefaceStream;


import java.util.Arrays;
import java.util.Comparator;


public class Lambda {
	public static String[] Ascending(String[] array) {
		Arrays.sort(array, (a, b) -> Integer.compare(a.length(), b.length()));
		return array;
	}

	public static void main(String args[]) {
		String[] str = { "james", "ana", "michael", "george", "eose" };
		Arrays.sort(str, Comparator.naturalOrder());
		System.out.println(Arrays.toString(str));
		Arrays.sort(str, Comparator.reverseOrder());
		System.out.println(Arrays.toString(str));
		String phone = "iPhone";
		int foo = phone.charAt(1);
		System.out.println(foo);
		for (String i : str) {
			if (i.startsWith("e"))
				System.out.println(i);
		}
		System.out.println(Arrays.toString(Ascending(str)));
	}

}
